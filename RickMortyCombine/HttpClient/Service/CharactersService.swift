//
//  CharactersService.swift
//  RickMortyCombine
//
//  Created by Anatoly Nevmerzhitsky on 06.04.2023.
//

import Alamofire
import Foundation
import Combine

class CharactersService {
    
    func getList() -> AnyPublisher<[CharacterDTO], AFError> {
        
        let client = HttpClient<CharactersResponse<CharacterDTO>>()
        return client.request(path: "/character").map { $0.results }.eraseToAnyPublisher()
    }
    
    func getMapperList() async throws -> [CharacterMapperDTO]? {
        let client = MapperHttpClient<CharactersMapperResponse>()
        do {
            let response = try await client.asyncRequest(path: "/character")
            return (response.characters)
        }
        catch {
            throw error
        }
    }
}

