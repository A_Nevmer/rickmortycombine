//
//  HttpClient.swift
//  RickMortyCombine
//
//  Created by Anatoly Nevmerzhitsky on 06.04.2023.
//

import Foundation
import Combine
import Alamofire

class HttpClient<T: Codable> {
    
    let baseUrl = "https://rickandmortyapi.com/api"
    
    public func request(path: String, method: HTTPMethod = .get, parameters: Parameters? = nil, encoding: ParameterEncoding = URLEncoding.default, headers: HTTPHeaders? = nil) -> AnyPublisher<T, AFError> {
        let jsonDecoder = JSONDecoder()
        jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
        return AF.request(
            baseUrl+path,
            method: method,
            parameters: parameters,
            encoding: encoding,
            headers: headers,
            interceptor: nil
        ).publishDecodable(type: T.self, decoder: jsonDecoder).value()
    }
}
