//
//  MapperHttpClient.swift
//  RickMortyCombine
//
//  Created by Anatoly Nevmerzhitsky on 07.04.2023.
//

import Foundation
import Alamofire
import ObjectMapper

class MapperHttpClient<T: BaseMappable> {
    
    let baseUrl = "https://rickandmortyapi.com/api"

    func asyncRequest(path: String, method: HTTPMethod = .get, parameters: Parameters? = nil, encoding: ParameterEncoding = URLEncoding.default, headers: HTTPHeaders? = nil) async throws -> T {
        
        try await withUnsafeThrowingContinuation { continuation in
            AF.request(
                baseUrl + path,
                method: method,
                parameters: parameters,
                encoding: encoding,
                headers: headers,
                interceptor: nil
            ).validate(contentType: ["application/json", "text/javascript"])
                .responseString { response in
                    if let error = response.error {
                        continuation.resume(throwing: error)
                    }
                    else {
                        switch response.result {
                        case .success:
                            if let value = response.value,
                               let result = Mapper<T>().map(JSONString: value) {
                                continuation.resume(returning: result)
                            }
                        case .failure(let error):
                            continuation.resume(throwing: error)
                        }
                    }
                }
        }
    }
}
