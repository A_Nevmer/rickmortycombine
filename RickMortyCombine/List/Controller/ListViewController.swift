//
//  ListViewController.swift
//  RickMortyCombine
//
//  Created by Anatoly Nevmerzhitsky on 06.04.2023.
//

import UIKit
import Combine

class ListViewController: UIViewController {
    
    // MARK: - Views
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.register(CharacterTableViewCell.self, forCellReuseIdentifier: CharacterTableViewCell.reuseId)
        tableView.dataSource = self
        return tableView
    }()
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.color = .black
        activityIndicator.hidesWhenStopped = true
        activityIndicator.stopAnimating()
        return activityIndicator
    }()
    
    // MARK: - Private properties
    
    private let viewModel: ListViewModel
    private var cancellables: Set<AnyCancellable> = []
    
    // MARK: - Init
    
    init(viewModel: ListViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        bind(viewModel: viewModel)
        viewModel.send(event: .onAppear)
    }
    
    // MARK: - Private methods
    
    private func bind(viewModel: ListViewModel) {
        viewModel.$state.removeDuplicates(by: { $0.state == $1.state })
            .sink { [weak self] state in
                self?.render(state: state)
            }
            .store(in: &cancellables)
    }
    
    private func render(state: ListViewModel.State) {
        switch state.state {
        case .idle:
            break
        case .loading:
            showIndicator()
        case .loaded:
            hideIndicator()
            tableView.reloadData()
        case .error(let error):
            hideIndicator()
            print(error)
        }
    }
    
    private func showIndicator() {
        activityIndicator.startAnimating()
    }
    
    private func hideIndicator() {
        activityIndicator.stopAnimating()
        
    }
}

extension ListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.state.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = viewModel.state.data[indexPath.row].cellForTableView(tableView: tableView, atIndexPath: indexPath)
        return cell
    }
}

// MARK: - Setup UI

private extension ListViewController {
    func setupUI() {
        view.backgroundColor = .white
        
        view.addSubview(tableView, anchors: [.leading(0), .trailing(0)])
        tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        view.addSubview(activityIndicator, anchors: [.centerX(0), .centerY(0)])

    }
}

