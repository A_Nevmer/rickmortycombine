//
//  CharacterTableCellModel.swift
//  RickMortyCombine
//
//  Created by Anatoly Nevmerzhitsky on 06.04.2023.
//

import Foundation
import UIKit

class CharacterTableCellModel: TableViewCompatible {
    
    // MARK: - Public properties
    
    let id: Int
    let name: String
    let gender: String
    let status: String
    let race: String

    // MARK: - Init
    
    init(character: Character) {
        id = character.id
        name = character.name
        gender = character.species
        status = character.status
        race = character.species
    }
    
    // MARK: - TableViewCompatible
    
    let reuseIdentifier: String = CharacterTableViewCell.reuseId
    var selected: Bool = false
    var editable: Bool = false
    var movable: Bool = false
    
    func cellForTableView(tableView: UITableView, atIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! CharacterTableViewCell
        cell.configure(withModel: self)
        
        return cell
    }
}

extension CharacterTableCellModel: Equatable {
    static func == (lhs: CharacterTableCellModel, rhs: CharacterTableCellModel) -> Bool {
        return lhs.id == rhs.id
    }
}
