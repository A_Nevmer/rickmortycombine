//
//  CharacterTableViewCell.swift
//  RickMortyCombine
//
//  Created by Anatoly Nevmerzhitsky on 06.04.2023.
//

import UIKit

class CharacterTableViewCell: UITableViewCell {
    
    static let reuseId = String(describing: CharacterTableViewCell.self)
    
    // MARK: - Views
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.borderColor = UIColor.black.cgColor
        view.layer.borderWidth = 1.0
        return view
    }()
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 10
        return stackView
    }()
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        return label
    }()
    private lazy var genderLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        return label
    }()
    private lazy var statusLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        return label
    }()
    
    
    // MARK: - Public properties
    
    var model: CharacterTableCellModel?
    
    // MARK: - Init
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    // MARK: - Public methods
    
    func configure(withModel model: CharacterTableCellModel) {
        nameLabel.text = model.name
        genderLabel.text = model.gender
        statusLabel.text = model.status
    }
    
    private func setupUI() {
        selectionStyle = .none
        contentView.addSubview(containerView, anchors: [.top(10), .bottom(-10), .leading(15), .trailing(-15)])
        containerView.addSubview(stackView, anchors: [.top(0), .bottom(0), .leading(0), .trailing(0)])
        stackView.addArrangedSubview(nameLabel)
        stackView.addArrangedSubview(genderLabel)
        stackView.addArrangedSubview(statusLabel)
    }
}


