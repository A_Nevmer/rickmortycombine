//
//  CharactersResponse.swift
//  RickMortyCombine
//
//  Created by Anatoly Nevmerzhitsky on 06.04.2023.
//

import Foundation

struct CharactersResponse<T: Codable>: Codable {
    let results: [T]
}
