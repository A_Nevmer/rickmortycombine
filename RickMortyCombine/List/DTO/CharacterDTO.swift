//
//  CharacterDTO.swift
//  RickMortyCombine
//
//  Created by Anatoly Nevmerzhitsky on 06.04.2023.
//

import Foundation

struct CharacterDTO: Codable {
    let id: Int
    let name: String
    let status: String
    let species: String
}
