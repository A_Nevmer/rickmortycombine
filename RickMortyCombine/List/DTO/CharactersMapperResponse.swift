//
//  CharactersMapperResponse.swift
//  RickMortyCombine
//
//  Created by Anatoly Nevmerzhitsky on 07.04.2023.
//

import Foundation
import ObjectMapper

class CharactersMapperResponse: Mappable {
    
    var characters: [CharacterMapperDTO]?
    
    required public init?(map: Map) { }
        
    public func mapping(map: Map) {
        characters  <- map["results"]
    }
}
