//
//  CharacterMapperDTO.swift
//  RickMortyCombine
//
//  Created by Anatoly Nevmerzhitsky on 07.04.2023.
//

import Foundation
import ObjectMapper

class CharacterMapperDTO: Mappable {
    
    var id: Int?
    var name: String?
    var status: String?
    var species: String?
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        id      <- map["id"]
        name    <- map["name"]
        status  <- map["status"]
        species <- map["species"]
    }
}
