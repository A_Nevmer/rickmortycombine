//
//  ListViewModel.swift
//  RickMortyCombine
//
//  Created by Anatoly Nevmerzhitsky on 06.04.2023.
//

import Foundation
import Combine

class ListViewModel: ObservableObject {
    
    // MARK: - Enums
    
    struct State: Equatable {
        enum State: Equatable {
            case idle
            case loading
            case loaded([Character])
            case error(String)
        }
        
        var state = State.idle
        var data: [CharacterTableCellModel] = []
    }
    
    enum Event {
        case onAppear
        case onDetails
        case onLoaded([Character])
        case onFailed(String)
    }
    
    // MARK: - Private properties
    
    @Published private(set) var state = State()
    private var bag = Set<AnyCancellable>()
    private let input = PassthroughSubject<Event, Never>()
    
    // MARK: - Init
    
    init() {
        Publishers.system(initial: state.state,
                          reduce: reduce,
                          scheduler: RunLoop.main,
                          feedbacks: [
                            whenLoading(),
                            userInput(input: input.eraseToAnyPublisher())
                        ]
        )
        .assign(to: \.state.state, on: self)
        .store(in: &bag)
    }
    
    deinit {
        bag.removeAll()
    }
    
    // MARK: - Public methods
    
    func send(event: Event) {
        input.send(event)
    }
    
    func setData(characters: [Character]) {
        state.data = characters.map({ CharacterTableCellModel(character: $0) })
    }
}

extension ListViewModel {
    
    func reduce(_ state: State.State, _ event: Event) -> State.State {
        switch state {
        case .idle:
            switch event {
            case .onAppear:
                return .loading
            default:
                return state
            }
        case .loading:
            switch event {
            case .onLoaded(let characters):
                self.state.data = characters.map({ CharacterTableCellModel(character: $0) })
                return .loaded(characters)
            case .onFailed(let error):
                return .error(error)
            default:
                return state
            }
        case .loaded:
            return state
        case .error:
            return state
        }
    }
    
    func userInput(input: AnyPublisher<Event, Never>) -> Feedback<State.State, Event> {
        Feedback { _ in input }
    }
}

// MARK: - Extension for objectMapper

extension ListViewModel {
//
    func loadData() -> AnyPublisher<[CharacterMapperDTO], Error> {
        return Future() { promise in
            Task {
                do {
                    let characters = try await CharactersService().getMapperList() ?? []
                    promise(Result<[CharacterMapperDTO], Error>.success(characters))
                }
                catch {
                    promise(Result<[CharacterMapperDTO], Error>.failure(error))
                }
            }
        }.eraseToAnyPublisher()
    }

    func whenLoading() -> Feedback<State.State, Event> {
        Feedback { (state: State.State) -> AnyPublisher<Event, Never> in
            guard case .loading = state else { return Empty().eraseToAnyPublisher() }
            return self.loadData()
                .map { $0.map(Character.init) }
                .map(Event.onLoaded)
                .catch { Just(Event.onFailed($0.localizedDescription)) }
                .eraseToAnyPublisher()
        }
    }
}

// MARK: - Extension for caddable

//extension ListViewModel {
//    static func whenLoading() -> Feedback<State.State, Event> {
//        Feedback { (state: State.State) -> AnyPublisher<Event, Never> in
//            guard case .loading = state else { return Empty().eraseToAnyPublisher() }
//
//            return CharactersService().getList()
//                .map { $0.map(Character.init) }
//                .map(Event.onLoaded)
//                .catch { Just(Event.onFailed($0.localizedDescription)) }
//                .eraseToAnyPublisher()
//        }
//    }
//}
