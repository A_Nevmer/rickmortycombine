//
//  Character.swift
//  RickMortyCombine
//
//  Created by Anatoly Nevmerzhitsky on 06.04.2023.
//

import Foundation

struct Character: Equatable {
    let id: Int
    let name: String
    let status: String
    let species: String
    
    init(character: CharacterDTO) {
        self.id = character.id
        self.name = character.name
        self.status = character.status
        self.species = character.species
    }
    
    init(character: CharacterMapperDTO) {
        self.id = character.id ?? -1
        self.name = character.name ?? ""
        self.status = character.status ?? ""
        self.species = character.species ?? ""
    }
}
